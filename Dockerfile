# multi-stage build
FROM node:16 as build-stage

WORKDIR fe

COPY . .

RUN npm install

RUN yarn add @atlaskit/css-reset@6.2.0

RUN npm run build


# bring artifacts
FROM nginx:1.16.0-alpine as production-stage

COPY --from=build-stage /fe/build /usr/share/nginx/html

RUN rm /etc/nginx/conf.d/default.conf

COPY nginx/nginx.conf /etc/nginx/conf.d

COPY nginx/certificate.crt /etc/ssl/certificate.crt

COPY nginx/private.key /etc/ssl/private.key

EXPOSE 443

CMD ["nginx", "-g", "daemon off;"]
